<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="relativeFileUri"/>
  <xsl:param name="clientDocumentId"/>

  <xsl:template match="/Document">
    <Document>
      <xsl:apply-templates/>
    </Document>
  </xsl:template>

  <xsl:template match="relativeFileUri">
    <relativeFileUri>
      <xsl:value-of select="$relativeFileUri"/>
    </relativeFileUri>
  </xsl:template>

  <xsl:template match="containers[1]">
    <containers>
      <xsl:apply-templates/>
      <xsl:call-template name="customValues"/>
    </containers>
  </xsl:template>

  <xsl:template name="customValues">
    <values>
      <value>
        <xsl:value-of select="$clientDocumentId"/>
      </value>
      <item>
        <description>Client document ID</description>
        <itemType>
          <description>any text data</description>
          <name>String</name>
        </itemType>
        <name>ClientDocumentId</name>
      </item>
    </values>
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>